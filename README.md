# Resumo
Este é um projeto node criado com o intuito educativo de demonstrar o potencial perigo do ataque ReDoS (Regular Expression Denial of Service) de forma prática

# Funcionamento
Esta aplicação consiste em uma API REST escrita em node utilizando express.
Ela simplemente recebe uma entrada em texto puro (text/plain) e testa essa entrada com uma regex que é vulnerável a Regex Dos.
 
Ela deve sempre retornar HTTP 202 

# Como executar
Para executar em testes locais utilize o comando `npm start` após instalar as dependencias utilizando `npm install`

# Como simular um ambiente containerizado 
1. Faça build da imagem 

```
docker build . -t {image}
```

2. Inicie um container com a imagem recém criada
```
docker run -d -p "8080:3000" --cpus 1 --rm {image}
```

> ### Explicando o comando;  
> -d não segura o comando enquanto o container estiver em execução  
> -p mapeia a porta local 8080 para a porta do container 3000  
> --cpus limita o uso de cpu do container a 1 core o que equivale a uma quota de 100ms em um período de 100ms (ver CFS)  
> --rm remove o container assim que ele for parado  


# Testando


## Requisições "boas"
No terminal abra n sessões e faça uma requisição utilizando a entrada "aaaaa" que ira simular um input comum.
Você pode utilizar o `watch` para fazer requisições em um intervalo x; exemplo

```
watch -n 0.1 -x curl -X POST -d "aaaaa" -i -w "@./curl_output_format.txt" -o /dev/null -H "Content-Type:text/plain" -s http://localhost:3000
```
_o arquivo curl_output_format.txt é um formato personalizado de saída para observar de forma mais precisa os tempos gastos na requisição_   


> ### Explicando o comando;  
> -n especifica o intervalo em segundos em que o comando deverá ser executado  
> -X define o método a ser utilizado  
> -d coloca um input   
> -w define o formato de saída do comando   
> -o ignora a saída padrão  
> -s host de destino  
> --header define o cabeçalho como texto puro  


## Requisições "más"

Abra um outro terminal a parte e utilize o mesmo comando utilizado na etapa anterior, porém utilizando o que iremos simular como uma string maliciosa `'a'.repeat(1266)` ou 'a' repetido 1266 vezes.

## Conclusões 
Ao fim dos testes, note que um input malicioso é capaz de tomar muito mais tempo de CPU do que os inputs "comuns".   

E ainda após enviar o input malicioso para a api as requisições boas são afetadas, porque todo recurso da máquina esta sendo ocupado para processar o input malicioso.

Isso pode consumir muita CPU da máquina hospedeira e causar indisponibilidade da aplicação

# Como prevenir ?

1. Utilize ferramentas como [Devina ReDoS Check](https://devina.io/redos-checker) para verificar se sua regex esta vulnerável

2. Sanitize entradas sempre que possível, estabeleça limites de caractéres

3. Se seu sistema permitir que inputs sejam utilizados em padrões de regex, crie regras estritas para impedir que uma regex vulnerável seja utilizada.



# Links
- [OWASP](https://owasp.org/www-community/attacks/Regular_expression_Denial_of_Service_-_ReDoS)