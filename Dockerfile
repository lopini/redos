FROM node:14.16.1-slim

WORKDIR /app

COPY * /app/

RUN npm i 
EXPOSE 3000

CMD npm start