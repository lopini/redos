const express = require('express');
const bodyParser = require('body-parser');


const REGEX_PATTERN = /^([a-zA-Z0-9])(([\-.]|[_]+)?([a-zA-Z0-9]+))*(@){1}[a-z0-9]+[.]{1}(([a-z]{2,3})|([a-z]{2,3}[.]{1}[a-z]{2,3}))$/g;
const app = express();
const PORT = 3000;

app.use(bodyParser.text());

app.post('/', (req, res) => {
    const input = req.body;
    if (input === undefined)
        res.status(400)
            .send(JSON.stringify(req.body));


    main(input);
    res.sendStatus(202);
});

app.listen(PORT, () => { console.log('now listening port '+ PORT); });

async function main(input){
    console.log('analyzing ' + input);

    regex = new RegExp(REGEX_PATTERN);
    start = new Date();
    result = input.match(REGEX_PATTERN);
    end = new Date();

    elapsed = end - start;

    console.log(`elapsed ${elapsed}ms`);
}
